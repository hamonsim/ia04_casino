module gitlab.utc.fr/hamonsim/ia04_casino

go 1.17

require github.com/stitchfix/mab v0.1.1

require (
	github.com/nleskiw/goplaycards v0.0.0-20170207042110-491f0f4aba66
	golang.org/x/exp v0.0.0-20200224162631-6cc2880d07d6 // indirect
	gonum.org/v1/gonum v0.8.2 // indirect
)
