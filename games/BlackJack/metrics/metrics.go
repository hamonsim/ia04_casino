package metrics

import (
	"fmt"
	"sync"

	"gitlab.utc.fr/hamonsim/ia04_casino/games/BlackJack/blackjack"
)

/*type Experiment struct {
	Players    []player.PlayerBase
	Dealer     BJ.CroupierBJ
	trials     int
	rawResults []int
}

func newExperiment(players []player.PlayerBase, dealer BJ.CroupierBJ, trials int, result []int) Experiment {
	return Experiment{players, dealer, trials, result}
}*/

/*func doMean(results []float64){
	for i,
}*/

func StartSimulation(nbTrial int, strat string, nbPlayers int) float64 {
	//trial := 100
	var wg sync.WaitGroup
	var results float64

	//Experiment := newExperiment(game, trial, result)
	for i := 0; i < nbTrial; i++ {
		wg.Add(1)
		go func() {
			//results = append(results, blackjack.StartGame())
			results += blackjack.StartGame(strat, nbPlayers)
			wg.Done()
		}()
	}
	wg.Wait()
	result := results / float64(nbTrial)
	fmt.Printf("pourcentage sur %d partie de la stratégie : %.2f", nbTrial, result)
	return result
	//time.Sleep(time.Minute)
}
