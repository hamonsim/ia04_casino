package blackjack

import (
	"fmt"

	"time"

	BJ "gitlab.utc.fr/hamonsim/ia04_casino/agt/dealer"
	player "gitlab.utc.fr/hamonsim/ia04_casino/agt/player"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type Game struct {
	//Deck    deck.Deck
	Players []player.PlayerBase
	Dealer  BJ.CroupierBJ
}

func NewGame(players []player.PlayerBase, dealer BJ.CroupierBJ) Game {
	return Game{players, dealer}
}

func StartGame(strat string, nbPlayers int) float64 {

	play := make(chan agt.RequestPlay)
	bet := make(chan agt.RequestBet)
	result := make(chan agt.ResultBlackjack)

	var players_id []agt.AgentID
	var players []player.PlayerBase
	nameString := "player N°"

	for i := 0; i < nbPlayers; i++ {
		fmt.Printf("Player n° %d \n", i)
		nameString += string(i)
		player := player.NewPlayerAgent(agt.AgentID(i), nameString, float64(100), play, bet, strat)
		//player.Start()
		players = append(players, player)
		players_id = append(players_id, player.Id)
	}

	croupier := BJ.NewCroupierAgent(agt.AgentID(1), "Croupier", players, play, bet, result)

	croupier.Start()
	//fmt.Println(d.CardsLeft())

	for _, plyr := range players {
		plyr.Start()
	}
	time.Sleep(10 * time.Second)
	close(bet)

	time.Sleep(10 * time.Second)
	close(play)
	resultStrat := <-result
	fmt.Println(resultStrat.Strat)
	fmt.Printf("Pourcentage reussite : %.2f\n", resultStrat.PourcentSucess)
	return resultStrat.PourcentSucess
	//time.Sleep(time.Minute)

}
