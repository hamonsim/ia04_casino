package blackjack

import (
	"fmt"

	"time"

	roulette "gitlab.utc.fr/hamonsim/ia04_casino/agt/dealerRoulette"
	player "gitlab.utc.fr/hamonsim/ia04_casino/agt/playerRoulette"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type Game struct {
	//Deck    deck.Deck
	Players []player.PlayerBase
	Dealer  roulette.Roulette
}

func NewGame(players []player.PlayerBase, dealer roulette.Roulette) Game {
	return Game{players, dealer}
}

func StartGame(strat string, nbPlayers int,wallet int) float64 {

	
	var players_id []agt.AgentID
	var players []player.PlayerBase

	play := make(chan agt.RequestPlayRoulette)
	bet := make(chan agt.RequestBetRoulette)
	result := make(chan agt.ResultRoulette)

	nameString := "player N°"

	for i := 0; i < nbPlayers; i++ {
		fmt.Printf("Player n° %d \n", i)
		nameString += string(i)
		player := player.NewPlayerAgent(agt.AgentID(i), nameString, float64(wallet), play, bet,strat)
		//player.Start()
		players = append(players, player)
		players_id = append(players_id, player.Id)
	}
	croupier := roulette.NewRouletteAgent(agt.AgentID(1), "Croupier", players, play, bet,result)
	fmt.Println(croupier)
	croupier.Start()
	for _, plyr := range players {
		plyr.Start()
	}

	time.Sleep(15 * time.Second)
	close(bet)

	
	close(play)
	//time.Sleep(time.Minute)
	//wg.Wait()

	resultStrat := <-result
	fmt.Println(resultStrat.Strat)
	fmt.Printf("Pourcentage reussite : %.6f\n", resultStrat.PourcentSucess)
	return resultStrat.PourcentSucess
	//time.Sleep(time.Minute)

}
