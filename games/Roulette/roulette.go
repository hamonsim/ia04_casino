package roulette

import(
	"fmt"
	"math/rand"
	"time"
	"strconv"
)


func roll ()(a int){
	rand.Seed(time.Now().UnixNano())
	b := rand.Intn(37)
	return b
}
// Nombre unique de 1 à 36, mise * 37
// Mise sur 2 chiffres côte à côte (cheval), mise *18
// Mise sur 3 numéros qui se touchent , mise *12
// Mise sur 4 numéros qui se touchent, mise *9
// Mise sur 6 numéros qui se touchent, mise * 6
// Mise sur 1/3 numéros , colonne alors mise *3
// Nombre pair, impair *2
//Mise sur 2/3 (à savoir 1 et 2, 2 et 3, 1 et 3) remporte x1.5

func play(bet int, numbers []int) (winBool bool,betWin float64) {
	size := len(numbers)
	win :=false
	random := roll()
	var betReturn float64
	fmt.Println("Roulette gives ",random)
	for _,v := range numbers{
		if(v==random){
			win=true
			fmt.Println("ITS A MATCH")
			switch size{
				case 1 :
					betReturn = float64(36 * bet)
				case 2 :
					betReturn = float64(18 * bet)
				case 3 :
					betReturn = float64(12 * bet)
				case 4 :
					betReturn = float64(9 * bet)
				case 6 :
					betReturn = float64(6 * bet)
				case 12 :
					betReturn = float64(3 * bet)
				case 18 :
					betReturn = float64(2 * bet)
				case 27 : 
					betReturn = float64(3/2 *bet)
				default:
					fmt.Println("Size is weird", size)
			}
			break
		}
	}
	if win==false{ 
		fmt.Println("Its a lose")
		betReturn =  float64(-bet)
	}
	return win,betReturn
	
}

func getInteger(prompt string) int {
	valid := true
	input := getString(prompt)
	integer, err := strconv.Atoi(input)
	if err != nil {
		valid = false
	}
	for valid == false {
		fmt.Println("Can't convert your answer into an integer.")
		input = getString(prompt)
		integer, err = strconv.Atoi(input)
		if err == nil {
			valid = true
		}
	}
	return integer
}


func getString(prompt string) string {
	fmt.Print(prompt)
	var input string
	fmt.Scanln(&input)
	return input
}


func getBet(wallet *float64) int {
	bet := 0
	valid := false
	for valid == false {
		valid = true
		bet = getInteger("How much would you like to bet ($5 increments)? ")
		if bet < 1 {
			valid = false
		}
		if float64(bet) > *wallet {
			valid = false
		}
		if bet%5 != 0 {
			valid = false
		}
		if valid == false {
			fmt.Println("Invalid bet.")
		}
	}
	*wallet = *wallet - float64(bet)
	return bet
}

func getPlayerAction()  []int{
	validInput := false
	input := ""
	var numbers [37] int
	var returns []int 

	for validInput == false {
		input = getString("[N]uméro plein ou [C]hances simples, [D]ouzaines  ")
		if input == "N"  {
			for validInput==false{
				numbers[0] = getInteger("Choose a number between 0 and 36 ")
				if(numbers[0] >= 0 && numbers[0] <= 37) {	
					validInput=true
				}
			} 
			returns = numbers[0:1]
			
		}
		if input == "C"  {
			input = getString("[P]air ou [I]mpair ")
			var start int
			if input =="P"{
				start=2
			}else if input =="I"{
				start=1}

			for  i:=0;i<=19;i++{
				numbers[i]= start + i*2
			}
			validInput=true

			returns = numbers[0:18]
		}
		if input == "D"  {
			input = getString("[P]remière (1-12) ou [D]euxième(13-24)  ou [T]roisième (25-36)")
			if input=="P"{
				for i:=0;i<13;i++{
					numbers[i]=i+1
					validInput=true
				}
			}

			if input=="D"{
				for i:=0;i<13;i++{
					numbers[i]=(i+1) +12
					validInput=true
				}
			}

			if input=="T"{
				for i:=0;i<13;i++{
					numbers[i]=(i+1) +24
					validInput=true
				}
			}

			returns = numbers[0:12]
		}
		if validInput == false {
			fmt.Println("Invalid option. H to Hit or S to Stand. ")
		}
	}

	return  returns
}



func main() {
//	fmt.Println(roll())

	wallet :=100.00
	bet :=0


	// While the player still has enough money for one bet
	for wallet > 5.0 {
		fmt.Println("=============================================================")
		// Minimum number of cards to play a hand from a single deck in worst case
		fmt.Printf("You have %.2f in your wallet.\n", wallet)
		bet = getBet(&wallet)
		fmt.Printf("\nYou bet: %d\n", bet)



		// Let the player play
		playerDone := false
		var action []int
		for playerDone == false {
			action = getPlayerAction()
			playerDone=true
		}
		fmt.Println("Let's ROLL")
		win,betWin := play(bet,action)
		if(win){
			fmt.Println("You won ", betWin)
		}else{
			fmt.Println("You lose ", betWin)
		}
		wallet += betWin
		
	}
	fmt.Println("You're out of money.")
	return
/*

	var numbersGame [37] int
	for i:=0;i<len(numbersGame);i++{
		numbersGame[i]=i;
	}
	fmt.Println(numbersGame[0])
	var numbers []int= numbersGame[1:19]
	fmt.Println("bet: ", play(5,numbers))

	fmt.Println("bet: ", play(5,numbers))
	test := getPlayerAction()
	
	for i:=0;i<len(test);i++{
		fmt.Println(test[i] ," taille" ,len(test))
	}
*/
}