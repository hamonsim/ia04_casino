package metrics

import (
	"fmt"
	"sync"

	roulette "gitlab.utc.fr/hamonsim/ia04_casino/games/Roulette/roulette"
)


func StartSimulation(nbTrial int, strat string, nbPlayers int,wallet int) float64 {
	
	var wg sync.WaitGroup
	var results float64

	//Experiment := newExperiment(game, trial, result)
	for i := 0; i < nbTrial; i++ {
		wg.Add(1)
		go func() {
			//results = append(results, blackjack.StartGame())
			results += roulette.StartGame(strat, nbPlayers,wallet)
			wg.Done()
		}()
	}
	wg.Wait()
	result := results / float64(nbTrial)
	fmt.Printf("pourcentage sur %d partie de la stratégie : %.5f", nbTrial, result*100)
	return result
	//time.Sleep(time.Minute)

}


