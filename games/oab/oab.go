package mab

import (
	"fmt"
	"math/rand"
)

type OAB struct {
	Last_draw []int
	mini_bet  int
	nb_wheels int
}

// Les combos gagnants
var winning_combos = map[int](map[int]int){
	1: {7: 1},
	2: {
		1: 10, 2: 20, 3: 30, 4: 40,
		5: 50, 6: 75, 7: 100,
	},
	3: {
		1: 250, 2: 500, 3: 1500, 4: 2500,
		5: 5000, 6: 7500, 7: 100000,
	},
}

// CREATE a new One Arm Bandit
func NewOABAgent(bet int) *OAB {
	return &OAB{mini_bet: bet, nb_wheels: 3}
}

func (bandit *OAB) Run() {
	var draw []int

	for i := 0; i < bandit.nb_wheels; i++ {
		number := rand.Intn(7)
		draw = append(draw, number)
	}
	bandit.Last_draw = draw
}

// pour lire le résultat il me faut une map avec chaque occurence
func checkOccurence(draw []int) map[int]int {
	occ := make(map[int]int)
	for _, num := range draw {
		occ[num] = occ[num] + 1
	}
	fmt.Println(draw)
	fmt.Println(occ)
	return occ
}

func (bandit *OAB) checkCombos() int {
	reward := 0
	occ := checkOccurence(bandit.Last_draw)
	// on a là une map [numéro] occurence
	// les winning combos sont sous la forme : [occurence] numéro
	for keys, values := range occ {
		fmt.Println(keys, values)
		if rew, ok := winning_combos[values][keys]; ok {
			reward += rew
		}
	}
	return reward
}

func (bandit *OAB) Play(bet int) int {

	bandit.Run()
	reward := bandit.checkCombos()
	return reward * bet
}
