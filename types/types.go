package types

import (
	deck "gitlab.utc.fr/hamonsim/ia04_casino/games"
)

//Crée un agent
type AgentID int

type Agent interface {
	Start()
}

type ReplyCroupier struct {
	Message       string
	Cards         []deck.Card
	CroupierCards deck.Card
	Done          bool
}

type ReplyCroupierPlay struct {
	Message       string
	Cards         []deck.Card
	CroupierCards deck.Card
	Done          bool
}

type ReplyCroupierBet struct {
	Message string
	cards   []deck.Card
}

//structure qui prends les info importantes
type RequestPlay struct {
	SenderID AgentID
	Play     string
	C        chan ReplyCroupier
}

type ReplyRoulette struct {
	Message   string
	AmountWin float64
	Win       bool
	Done      bool
}

type RequestPlayRoulette struct {
	SenderID AgentID
	Play     string
	C        chan ReplyRoulette
}

type RequestBetRoulette struct {
	SenderID AgentID
	Bet      int
	Values   []int
	C        chan ReplyRoulette
}

type RequestBet struct {
	SenderID AgentID
	Bet      int
	C        chan ReplyCroupier
}

type ResultBlackjack struct {
	Strat          string
	PourcentSucess float64
}

type Numbers struct {
	Num1 float64 `json:"num1"`
	Num2 float64 `json:"num2"`
}

type NumsResponseData struct {
	Add float64 `json:"add"`
	Mul float64 `json:"mul"`
	Sub float64 `json:"sub"`
	Div float64 `json:"div"`
}

type RequestSimulation struct {
	Trials      int    `json:"trials"`
	Strategie   string `json:"strategie"`
	BjPlayer    int    `json:"bjplayer"`
	StrategieRl string `json:"strategierl"`
	RlPlayer    int    `json:"rlplayer"`
	Wallet      int    `json:"wallet"`
}

type SimulationResponse struct {
	State    string  `json:"state"`
	ResultBj float64 `json:"res"`
	ResultRl float64 `json:"resrl"`
}

type ResultRoulette struct {
	Strat          string
	PourcentSucess float64
}
