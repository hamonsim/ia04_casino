package main

import (
	"fmt"

	cagt "gitlab.utc.fr/hamonsim/ia04_casino/agt/casino"
)

func main() {
	server := cagt.NewCasinoAgent(":8000")
	server.Start()

	fmt.Scanln()
}
