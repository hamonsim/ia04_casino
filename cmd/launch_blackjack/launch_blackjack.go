package main

/*import (
	"fmt"
	"time"

	BJ "gitlab.utc.fr/hamonsim/ia04_casino/agt/dealer"
	player "gitlab.utc.fr/hamonsim/ia04_casino/agt/player"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

//"fmt"

//var d deck.Deck

func main() {
	//d.Initialize()
	//d.Shuffle()
	//fmt.Println(d.CardsLeft())
	wallet := 100
	strat := "soft16"

	var players_id []agt.AgentID
	var players []player.PlayerBase
	result := make(chan agt.ResultBlackjack)
	play := make(chan agt.RequestPlay)
	bet := make(chan agt.RequestBet)
	nameString := "player N°"

	//wg.Add(len(players) + 1)

	for i := 0; i < 1; i++ {
		fmt.Printf("Player n° %d \n", i)
		nameString += string(i)
		player := player.NewPlayerAgent(agt.AgentID(i), nameString, float64(wallet), play, bet, strat)
		//player.Start()
		players = append(players, player)
		players_id = append(players_id, player.Id)
	}
	croupier := BJ.NewCroupierAgent(agt.AgentID(1), "Croupier", players, play, bet, result)

	croupier.Start()

	//fmt.Println(d.CardsLeft())

	for _, plyr := range players {
		plyr.Start()
	}

	time.Sleep(10 * time.Second)
	//wg.Wait()
	//fmt.Println(croupier.Deck.CardsLeft())
	close(bet)

	time.Sleep(10 * time.Second)
	close(play)
	//BlackJack := BJ.NewBlackJackAgent(agt.AgentID(1), "BlackJack", players)
	//fmt.Printf("Commencement de la partie de BLACKJACK\n")
	//fmt.Printf("======================================\n")
	//fmt.Printf("Blackjack N° %d\nNb Joueurs : %d \n", BlackJack.Id, len(BlackJack.Players))
	resultStrat := <-result
	fmt.Println(resultStrat.Strat)
	fmt.Printf("Pourcentage reussite : %.2f", resultStrat.PourcentSucess)
	time.Sleep(time.Minute)

	//wg.Wait()
}*/

import (
	"gitlab.utc.fr/hamonsim/ia04_casino/games/BlackJack/metrics"
)

func main() {
	metrics.StartSimulation(100, "mainSouple", 2)
}
