package main
/*
import (
	//"fmt"
	"fmt"
	"time"

	Roulette "gitlab.utc.fr/hamonsim/ia04_casino/agt/dealerRoulette"
	player "gitlab.utc.fr/hamonsim/ia04_casino/agt/playerRoulette"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

var Name = []string{"Emile", "Hugo", "Jean", "Eliot", "Jérémy"}

//var d deck.Deck

func main() {
	wallet := 100
	var players_id []agt.AgentID
	var players []player.PlayerBase
	result := make(chan agt.ResultRoulette)
	play := make(chan agt.RequestPlayRoulette)
	bet := make(chan agt.RequestBetRoulette)
	strat := "Random"


	for i := 0; i < 2; i++ {
		fmt.Printf("Player n° %d \n", i)
		player := player.NewPlayerAgent(agt.AgentID(i), Name[i], float64(wallet), play, bet,strat)
		//player.Start()
		players = append(players, player)
		players_id = append(players_id, player.Id)
	}
	croupier := Roulette.NewRouletteAgent(agt.AgentID(1), "Croupier", players, play, bet,result)
	fmt.Println(croupier)
	croupier.Start()
	for _, plyr := range players {
		plyr.Start()
	}

	time.Sleep(5 * time.Second)
	//fmt.Println(croupier.Deck.CardsLeft())
	close(bet)

	time.Sleep(5 * time.Second)
	close(play)
	time.Sleep(time.Minute)
	//wg.Wait()

	return
}
*/
import (
	"gitlab.utc.fr/hamonsim/ia04_casino/games/Roulette/metrics"
)

func main() {
	metrics.StartSimulation(50,"Martingale",2,10000)
}