package client

import (
	"fmt"
	"strconv"
	"time"

	strategie "gitlab.utc.fr/hamonsim/ia04_casino/agt/strategies"
	deck "gitlab.utc.fr/hamonsim/ia04_casino/games"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type PlayerBase struct {
	Id     agt.AgentID
	Name   string
	Wallet float64
	Resp   chan agt.ReplyCroupier
	Bet    chan agt.RequestBet
	Play   chan agt.RequestPlay
	strat  string
}

func NewPlayerAgent(id agt.AgentID, name string, wallet float64, play chan agt.RequestPlay, bet chan agt.RequestBet, strat string) PlayerBase {
	resp := make(chan agt.ReplyCroupier)
	return PlayerBase{id, name, wallet, resp, bet, play, strat}
}

func getString(prompt string) string {
	fmt.Print(prompt)
	var input string
	fmt.Scanln(&input)
	return input
}

func getInteger(prompt string) int {
	valid := true
	input := getString(prompt)
	integer, err := strconv.Atoi(input)
	if err != nil {
		valid = false
	}
	for valid == false {
		fmt.Println("Can't convert your answer into an integer.")
		input = getString(prompt)
		integer, err = strconv.Atoi(input)
		if err == nil {
			valid = true
		}
	}
	return integer
}

// handTotal returns the numerical value of a Blackjack hand
func handTotal(hand []deck.Card) int {
	total := 0
	numberOfAces := 0
	for _, card := range hand {
		if card.Value.Name == "Ace" {
			numberOfAces = numberOfAces + 1
		} else {
			if card.Facecard() {
				total = total + 10
			} else {
				total = total + card.Value.Value
			}
		}
	}

	// If there's at least one Ace, deal with it.
	// In multi-shoe decks, there could be many Aces (more than 4) in a hand.
	if numberOfAces > 0 {
		// All but the last Ace must be a one, because 11 + 11 = 22 (bust)
		// This loop shouldn't run if there's only one Ace
		for numberOfAces > 1 {
			total = total + 1
			numberOfAces = numberOfAces - 1
		}
		// There should now only be one Ace
		// if the last Ace being 11 doesn't cause a bust, make it an 11
		if total+11 > 21 {
			total = total + 1
		} else {
			// If 11 doesn't cause a bust, make it worth 11
			total = total + 11
		}
	}
	return total
}

func printHand(hand []deck.Card) {
	for _, card := range hand {
		fmt.Printf("%s  ", card.ToStr())
	}
	fmt.Printf(" Total: %d\n", handTotal(hand))
}

func printPlayerHand(hand []deck.Card) {
	fmt.Printf("Player Hand: ")
	printHand(hand)
}

func (ag *PlayerBase) GiveBet() {
	bet := 0
	valid := false
	for valid == false {
		valid = true
		fmt.Printf("I would like to bet 5$\n")
		bet = 5
		if bet < 5 {
			valid = false
		}
		if float64(bet) > ag.Wallet {
			valid = false
		}
		if bet%5 != 0 {
			valid = false
		}
		if valid == false {
			fmt.Println("Invalid bet.")
		}
	}
	ag.Wallet = ag.Wallet - float64(bet)
	ag.Bet <- agt.RequestBet{ag.Id, bet, ag.Resp}
}

func (ag *PlayerBase) GivePlayerAction(cards []deck.Card, dealerCard deck.Card) {
	input := ""
	if ag.strat == "soft16" {
		if handTotal(cards) > 16 {
			input = "S"

		} else {
			input = "H"
		}
	} else if ag.strat == "mainSouple" {
		input = strategie.MainSouple(cards, dealerCard)
	} else if ag.strat == "mainRigide" {
		input = strategie.MainRigide(cards, dealerCard)
	}

	fmt.Printf("Player N° %d play : %s\n", ag.Id, input)
	ag.Play <- agt.RequestPlay{ag.Id, input, ag.Resp}
}

func (ag PlayerBase) Start() {
	go func() {
		fmt.Printf("You have %.2f in your wallet.\n", ag.Wallet)
		ag.GiveBet()
		answer := <-ag.Resp
		message := answer.Message
		cards := answer.Cards
		dealerCard := answer.CroupierCards
		fmt.Printf("Player n°%d has received %q \n", ag.Id, message)
		time.Sleep(5 * time.Second)

		playerDone := true
		for playerDone {
			printPlayerHand(cards)
			ag.GivePlayerAction(cards, dealerCard)
			reply := <-ag.Resp

			message = reply.Message

			if reply.Cards != nil {
				fmt.Println(reply.Cards[0])
				cards = append(cards, reply.Cards[0])
			}
			//cards = append(cards, reply.Cards[0])
			playerDone = reply.Done
			fmt.Printf("Player N°%d has receive %s\n", ag.Id, message)
		}

	}()

}

/*func (ag PlayerBase) Start() {
	fmt.Printf("You have %.2f in your wallet.\n", ag.Wallet)
	ag.GiveBet()
	answer := <-ag.Resp
	message := answer.Message
	cards := answer.Cards

	fmt.Printf("Player n°%d has received %q \n", ag.Id, message)
	// printPlayerHand(cards)
	time.Sleep(5 * time.Second)

	playerDone := true
	for playerDone {
		printPlayerHand(cards)
		ag.GivePlayerAction(cards)
		reply := <-ag.Resp

		message = reply.Message

		if reply.Cards != nil {

			printHand(reply.Cards)
			cards = append(cards, reply.Cards[0])
		}
		//cards = append(cards, reply.Cards[0])
		playerDone = reply.Done
		fmt.Printf("Player N°%d has receive %s\n", ag.Id, message)
	}
}*/
