package dealer

import (
	"fmt"
	"time"

	agtPlayer "gitlab.utc.fr/hamonsim/ia04_casino/agt/player"
	deck "gitlab.utc.fr/hamonsim/ia04_casino/games"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

var d deck.Deck

//Agent blackJack
type BlackJack struct {
	Id      agt.AgentID
	Name    string
	Players []agt.AgentID
	//Players []agtPlayer.PlayerBase
	//Bet           chan agt.RequestBet
	//Play          chan agt.RequestPlay
	Bet_players   map[agt.AgentID]int
	Count_players map[agt.AgentID][]deck.Card
}

type CroupierBJ struct {
	Id   agt.AgentID
	Name string
	//Players        []agt.AgentID
	Players        []agtPlayer.PlayerBase
	Bet_players    map[agt.AgentID]int
	Deck           deck.Deck
	Play           chan agt.RequestPlay
	Bet            chan agt.RequestBet
	Result         chan agt.ResultBlackjack
	Count_players  map[agt.AgentID][]deck.Card
	Count_croupier []deck.Card
}

func NewCroupierAgent(id agt.AgentID, name string, players []agtPlayer.PlayerBase, play chan agt.RequestPlay, bet chan agt.RequestBet, result chan agt.ResultBlackjack) CroupierBJ {
	bet_players := make(map[agt.AgentID]int)
	count_players := make(map[agt.AgentID][]deck.Card)
	var count_croupier []deck.Card
	for _, plyr := range players {
		bet_players[plyr.Id] = 0
		count_players[plyr.Id] = nil
	}
	d.Initialize()
	d.Shuffle()
	return CroupierBJ{id, name, players, bet_players, d, play, bet, result, count_players, count_croupier}
}

func NewBlackJackAgent(id agt.AgentID, name string, players []agt.AgentID) BlackJack {
	bet_players := make(map[agt.AgentID]int)
	count_players := make(map[agt.AgentID][]deck.Card)
	for _, id := range players {
		bet_players[id] = 0
		count_players[id] = nil
	}
	//bet := make(chan agt.RequestBet)
	//play := make(chan agt.RequestPlay)
	return BlackJack{id, name, players, bet_players, count_players}
}

// handTotal returns the numerical value of a Blackjack hand
func handTotal(hand []deck.Card) int {
	total := 0
	numberOfAces := 0
	for _, card := range hand {
		if card.Value.Name == "Ace" {
			numberOfAces = numberOfAces + 1
		} else {
			if card.Facecard() {
				total = total + 10
			} else {
				total = total + card.Value.Value
			}
		}
	}

	// If there's at least one Ace, deal with it.
	// In multi-shoe decks, there could be many Aces (more than 4) in a hand.
	if numberOfAces > 0 {
		// All but the last Ace must be a one, because 11 + 11 = 22 (bust)
		// This loop shouldn't run if there's only one Ace
		for numberOfAces > 1 {
			total = total + 1
			numberOfAces = numberOfAces - 1
		}
		// There should now only be one Ace
		// if the last Ace being 11 doesn't cause a bust, make it an 11
		if total+11 > 21 {
			total = total + 1
		} else {
			// If 11 doesn't cause a bust, make it worth 11
			total = total + 11
		}
	}
	return total
}

func printHand(hand []deck.Card) {
	for _, card := range hand {
		fmt.Printf("%s  ", card.ToStr())
	}
	fmt.Printf(" Total: %d\n", handTotal(hand))
}

func printPlayerHand(hand []deck.Card) {
	fmt.Printf("Player Hand: ")
	printHand(hand)
}

func printDealerHand(hand []deck.Card, hideFirst bool) {
	fmt.Printf("Dealer Hand: ")
	if hideFirst {
		fmt.Printf("XX  %s  \n", hand[1].ToStr())
	} else {
		printHand(hand)
	}
}

// Returns true if a hand is bust / over 21
func isBust(hand []deck.Card) bool {
	if handTotal(hand) > 21 {
		return true
	}
	return false
}

// Returns true if a hand is a Blacjack (Ace + [10 | K | Q | J])
func isBlackjack(hand []deck.Card) bool {
	// A Blackjack is exactly one Ace and Exactly one 10, K, Q, or A
	if len(hand) != 2 {
		return false
	}
	// In the goplaycards library, the values enumerate from 2 to 14.
	// J = 11, Q = 12, K = 13, Ace = 14
	if hand[0].Value.Name == "Ace" {
		if hand[1].Value.Value >= 10 && hand[1].Value.Value <= 13 {
			return true
		}
	}
	if hand[1].Value.Name == "Ace" {
		if hand[0].Value.Value >= 10 && hand[0].Value.Value <= 13 {
			return true
		}
	}
	return false
}

func (ag *CroupierBJ) handlePlayerPlay(play agt.RequestPlay, done bool) {
	action := play.Play
	resp := "Votre action a bien été prise en compte, "
	var drawnCards []deck.Card
	var dealerHand deck.Card
	if action == "H" {
		drawnCards, _ = ag.Deck.Draw(1)

		ag.Count_players[play.SenderID] = append(ag.Count_players[play.SenderID], drawnCards[0])

		if isBust(ag.Count_players[play.SenderID]) {
			fmt.Println("Bust")
			resp += "Vous avez dépasser 21 vous perdez"
			done = false
			drawnCards = nil
		}
	}
	if action == "S" {
		resp += "Vous avez stand vous vous arretez la"
		done = false
		//drawnCards = nil
	}

	Data := agt.ReplyCroupier{
		resp,
		drawnCards,
		dealerHand,
		done,
	}
	play.C <- Data

}

func (ag *CroupierBJ) handlePlayerBet(bet agt.RequestBet, dealerHand deck.Card) {
	amount := bet.Bet
	ag.Bet_players[bet.SenderID] += amount
	response := "Votre réponse à bien été prise en compte"
	playerhand, _ := ag.Deck.Draw(2)
	ag.Count_players[bet.SenderID] = playerhand

	Data := agt.ReplyCroupier{
		response,
		playerhand,
		dealerHand,
		true,
	}
	bet.C <- Data
}

func (ag *CroupierBJ) handleBlackJack() {
	for key, element := range ag.Count_players {
		if isBlackjack(ag.Count_croupier) || isBlackjack(element) {

			if isBlackjack(ag.Count_croupier) && isBlackjack(element) {
				fmt.Printf("Both the Player n°%d and the Dealer have Blackjack. Hand is a push.\n", key)
				//TODO : gérer donne argent
				//wallet = wallet + float64(bet)
				continue
			}

			if isBlackjack(ag.Count_croupier) {
				fmt.Println("Dealer has Blackjack. Player loses this hand.")
				continue
			}

			if isBlackjack(element) {
				//TODO : gérer donne argent
				winnings := float64(ag.Bet_players[key]) * 2.5
				fmt.Printf("Player has Blackjack. Player wins $%.2f.\n", winnings)
				//key.wallet = wallet + winnings
				continue
			}
		}
	}
}

func (ag *CroupierBJ) DealerPlay() {

	dealerDone := false
	for dealerDone == false {
		if handTotal(ag.Count_croupier) >= 17 {
			dealerDone = true
			continue
		}
		newCards, err := ag.Deck.Draw(1)
		if err != nil {
			panic(err)
		}
		ag.Count_croupier = append(ag.Count_croupier, newCards[0])
	}

}

//TODO gerer egalité mieux
func (ag *CroupierBJ) handleEndGame() map[agt.AgentID]bool {
	winners := make(map[agt.AgentID]bool)
	for id, cards := range ag.Count_players {
		if isBust(cards) {
			fmt.Printf("Player N°%d busts. Dealer wins.\n", id)
			winners[id] = false
			continue
		}
		if handTotal(ag.Count_croupier) > 21 {
			fmt.Printf("Dealer busts.  Player N°%d wins.\n", id)
			//wallet = wallet + float64(bet*2)
			winners[id] = true
			continue
		}

		if handTotal(cards) > handTotal(ag.Count_croupier) {
			fmt.Printf("Player's hand beats the dealer. Player N°%d wins.\n", id)
			//wallet = wallet + float64(bet*2)
			winners[id] = true
			continue
		}

		if handTotal(cards) == handTotal(ag.Count_croupier) {
			fmt.Println("Push.")
			winners[id] = true
			//wallet = wallet + float64(bet)
			continue
		}

		if handTotal(cards) < handTotal(ag.Count_croupier) {
			fmt.Printf("Dealer wins. Player N°%d loses.\n", id)
			winners[id] = false
			continue
		}
		fmt.Println("There was a problem determining the winner.")
	}
	return winners

}

func (ag CroupierBJ) Start() {
	go func() {
		//nb_players := len(ag.Players)
		//ag.Deck.Initialize()
		ag.Deck.Shuffle()
		Dealerhand, _ := ag.Deck.Draw(2)
		ag.Count_croupier = Dealerhand
		faceCard := Dealerhand[0]
		//Bet and initialize deck for everyplayer
		ok := true
		for ok {
			select {
			case req, open := <-ag.Bet:
				{
					if !open {
						ok = false
					} else {
						fmt.Printf("Le croupier n° %d has received a bet of %d from %d\n", ag.Id, req.Bet, req.SenderID)
						go ag.handlePlayerBet(req, faceCard)
					}
					//fmt.Println(ag.Deck.CardsLeft())
				}

			}

		}
		//Dealerhand, _ := ag.Deck.Draw(2)
		//ag.Count_croupier = Dealerhand
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Bet summary:\n")
		for key, element := range ag.Bet_players {
			fmt.Printf("Player N°%d have bet %d $\n", key, element)
		}
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Cards summary :\n")
		for key, element := range ag.Count_players {
			fmt.Printf("N°%d: ", key)
			printPlayerHand(element)
		}
		printDealerHand(ag.Count_croupier, true)
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Wallet Summary\n")
		for _, plyr := range ag.Players {
			fmt.Printf("N°%d : has %2.f\n", plyr.Id, plyr.Wallet)
		}
		//fmt.Println(ag.Deck.CardsLeft())

		// Handle all Blackjack
		ag.handleBlackJack()

		//let players play

		// Let players play
		//action := <-ag.Play
		//go ag.handlePlayerPlay(action)

		ok = true
		for ok {
			select {
			case action, open := <-ag.Play:
				{
					if !open {
						ok = false
					} else {
						fmt.Printf("Le croupier n° %d has received a move : %s from %d\n", ag.Id, action.Play, action.SenderID)
						go ag.handlePlayerPlay(action, true)
					}
				}
			}

		}
		time.Sleep(5 * time.Second)
		ag.DealerPlay()
		isWinners := ag.handleEndGame()

		fmt.Printf("---------------------------------------------------------------\n")
		fmt.Printf("Game Summary\n")
		for key, element := range ag.Count_players {
			fmt.Printf("N°%d: ", key)
			printPlayerHand(element)
		}
		printDealerHand(ag.Count_croupier, false)
		var win float64
		for id, isWinner := range isWinners {
			if isWinner {
				win += 1
				fmt.Printf("Le player %d a gagné contre le dealer\n", id)
			} else {
				fmt.Printf("Le player %d a perdu contre le dealer\n", id)

			}
		}
		var pourcentSucees float64
		pourcentSucees = float64(win/float64(len(ag.Players))) * 100
		fmt.Printf("Pourcent sucess of the strategy during this game :%.2f\n", pourcentSucees)
		fmt.Printf("---------------------------------------------------------------\n")

		strat := "soft16"
		data := agt.ResultBlackjack{
			strat,
			pourcentSucees,
		}
		ag.Result <- data

	}()
}

/*func (ag CroupierBJ) Start() {
	//nb_players := len(ag.Players)
	//ag.Deck.Initialize()
	ag.Deck.Shuffle()
	//Bet and initialize deck for everyplayer
	ok := true
	for ok {
		select {
		case req, open := <-ag.Bet:
			{
				if !open {
					ok = false
				} else {
					fmt.Printf("Le croupier n° %d has received a bet of %d from %d\n", ag.Id, req.Bet, req.SenderID)
					go ag.handlePlayerBet(req)
				}
				//fmt.Println(ag.Deck.CardsLeft())
			}
			//fmt.Println(open)
		}

	}
	Dealerhand, _ := ag.Deck.Draw(2)
	ag.Count_croupier = Dealerhand
	fmt.Printf("--------------------------------------------\n")
	fmt.Printf("Bet summary :\n")
	for key, element := range ag.Bet_players {
		fmt.Printf("Player N°%d have bet %d $\n", key, element)
	}
	fmt.Printf("--------------------------------------------\n")
	fmt.Printf("Cards summary :\n")
	for key, element := range ag.Count_players {
		fmt.Printf("N°%d: ", key)
		printPlayerHand(element)
	}
	printDealerHand(ag.Count_croupier, true)
	fmt.Printf("--------------------------------------------\n")
	fmt.Printf("Wallet Summary\n")
	for _, plyr := range ag.Players {
		fmt.Printf("N°%d : has %2.f\n", plyr.Id, plyr.Wallet)
	}
	//fmt.Println(ag.Deck.CardsLeft())

	// Handle all Blackjack
	ag.handleBlackJack()

	//let players play

	// Let players play
	//action := <-ag.Play
	//go ag.handlePlayerPlay(action)

	ok = true
	for ok {
		select {
		case action, open := <-ag.Play:
			{
				if !open {
					ok = false
				}
				fmt.Printf("Le croupier n° %d has received a move : %s from %d\n", ag.Id, action.Play, action.SenderID)
				go ag.handlePlayerPlay(action, true)
			}
		}

		//action := <-ag.Play
		//fmt.Printf("Le croupier n° %d has received a move : %s from %d\n", ag.Id, action.Play, action.SenderID)
		//go ag.handlePlayerPlay(action, true)
	}
	time.Sleep(5 * time.Second)
	ag.DealerPlay()
	isWinners := ag.handleEndGame()

	fmt.Printf("--------------------------------------------\n")
	fmt.Printf("Game Summary\n")
	for key, element := range ag.Count_players {
		fmt.Printf("N°%d: ", key)
		printPlayerHand(element)
	}
	printDealerHand(ag.Count_croupier, false)
	var win float64
	for id, isWinner := range isWinners {
		if isWinner {
			win += 1
			fmt.Printf("Le player %d a gagné contre le dealer\n", id)
		} else {
			fmt.Printf("Le player %d a perdu contre le dealer\n", id)

		}
	}
	fmt.Println(win)
	fmt.Println(len(ag.Players))
	var pourcentSucees float64
	pourcentSucees = float64(win/float64(len(ag.Players))) * 100
	fmt.Printf("Pourcent sucess of the strategy during this game :%.2f\n", pourcentSucees)
	strat := "soft16"
	data := agt.ResultBlackjack{
		strat,
		pourcentSucees,
	}
	ag.Result <- data
}*/
