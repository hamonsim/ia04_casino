package strategie

//import "github.com/nleskiw/goplaycards/deck"
import (
	deck "gitlab.utc.fr/hamonsim/ia04_casino/games"
)

func MainSouple(hand []deck.Card, carte_croupier deck.Card) (decision string) { // cas où une de mes cartes est un as
	total_sans_As := handTotal(hand) - 11
	if total_sans_As >= 2 && total_sans_As <= 6 {
		return "H"
	} else if total_sans_As == 7 {
		if carte_croupier.Value.Value == 2 || carte_croupier.Value.Value == 7 || carte_croupier.Value.Value == 8 {
			return "S"
		} else {
			return "H"
		}
	} else if total_sans_As >= 8 {
		return "S"
	} else {
		return "H"
	}
}

func MainRigide(hand []deck.Card, carte_croupier deck.Card) (decision string) { // cas où je n'ai aucun as
	total := handTotal(hand)
	if total <= 11 {
		return "H"
	} else if total == 12 {
		if carte_croupier.Value.Value >= 4 && carte_croupier.Value.Value <= 6 {
			return "S"
		} else {
			return "H"
		}
	} else if total >= 13 && total <= 16 {
		if carte_croupier.Value.Value >= 2 && carte_croupier.Value.Value <= 6 {
			return "S"
		} else {
			return "H"
		}
	} else if total >= 17 {
		return "S"
	} else {
		return "H"
	}
}

// handTotal returns the numerical value of a Blackjack hand
func handTotal(hand []deck.Card) int {
	total := 0
	numberOfAces := 0
	for _, card := range hand {
		if card.Value.Name == "Ace" {
			numberOfAces = numberOfAces + 1
		} else {
			if card.Facecard() {
				total = total + 10
			} else {
				total = total + card.Value.Value
			}
		}
	}

	// If there's at least one Ace, deal with it.
	// In multi-shoe decks, there could be many Aces (more than 4) in a hand.
	if numberOfAces > 0 {
		// All but the last Ace must be a one, because 11 + 11 = 22 (bust)
		// This loop shouldn't run if there's only one Ace
		for numberOfAces > 1 {
			total = total + 1
			numberOfAces = numberOfAces - 1
		}
		// There should now only be one Ace
		// if the last Ace being 11 doesn't cause a bust, make it an 11
		if total+11 > 21 {
			total = total + 1
		} else {
			// If 11 doesn't cause a bust, make it worth 11
			total = total + 11
		}
	}
	return total
}

// return True if there is an Ace, False if not
func ifThereIsAce(hand []deck.Card) bool {
	for _, card := range hand {
		if card.Value.Name == "Ace" {
			return true
		}
	}
	return false
}
