package strategie

import (
	"fmt"
	"sync"

	agtPlayer "gitlab.utc.fr/hamonsim/ia04_casino/agt/playerRoulette"
	player "gitlab.utc.fr/hamonsim/ia04_casino/agt/playerRoulette"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type Roulette struct {
	Id   agt.AgentID
	Name string
	//Players        []agt.AgentID
	Players       []agtPlayer.PlayerBase
	Bet_players   map[agt.AgentID]int
	Play          chan agt.RequestPlayRoulette
	Bet           chan agt.RequestBetRoulette
	Count_players map[agt.AgentID]int
	Mu            sync.Mutex
	Tour          int
	Roll          int
}

func Martingale(ag *player.PlayerBase, valueRoulette int, bet_init int, bet_numbers []int) (winBool, betReturn float64) {

	bet := bet_init
	for float64(bet) <= ag.Wallet {
		winBool, _ := play(valueRoulette, bet_init, bet_numbers)
		if winBool == false {
			ag.Wallet -= float64(bet)
			bet *= 2
		}
	}
	return winBool, betReturn
}

func Labouchere(ag *player.PlayerBase, valueRoulette int, bet_numbers []int) (winBool, betReturn float64) {
	var gain int
	goal := 100
	var bet int
	var goal_sequence = make([]int, 10)
	start := 0
	end := len(goal_sequence)
	for i := range goal_sequence {
		goal_sequence[i] = 10
	}

	for gain < goal {
		// if float64(bet) <= ag.Wallet {}
		bet = goal_sequence[start] + goal_sequence[end]
		winBool, _ := play(valueRoulette, bet, bet_numbers)
		if winBool {
			start += 1
			end -= 1
		} else if !winBool {
			end += 1
			goal_sequence[end] = bet
		}
	}

	return winBool, betReturn
}

func play(valueRoulette int, bet int, numbers []int) (winBool bool, betWin float64) {
	size := len(numbers)
	win := false
	random := valueRoulette
	var betReturn float64
	fmt.Println("Roulette gives ", random)
	for _, v := range numbers {
		if v == random {
			win = true
			fmt.Println("ITS A MATCH")
			switch size {
			case 1:
				//Unique value
				betReturn = float64(36 * bet)
			case 2:
				//Cheval, deux cases qui se touchent
				betReturn = float64(18 * bet)
			case 3:
				//Transversale, trois case qui se touchent
				betReturn = float64(12 * bet)
			case 4:
				//Carré, quatre cases qui se touchent
				betReturn = float64(9 * bet)
			case 6:
				//Sizain, six cases qui se touchent
				betReturn = float64(6 * bet)
			case 12:
				//Douzaine , 1-12.13-24.25-36
				betReturn = float64(3 * bet)
			case 18:
				betReturn = float64(2 * bet)
			case 27:
				betReturn = float64(3 / 2 * bet)
			default:
				fmt.Println("Size is weird", size)
			}
			break
		}
	}
	if win == false {
		fmt.Println("Its a lose")
		betReturn = float64(-bet)
	}
	return win, betReturn

}
