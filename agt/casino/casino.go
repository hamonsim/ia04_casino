package casino

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net/http"
	"sync"
	"time"

	mbj "gitlab.utc.fr/hamonsim/ia04_casino/games/Blackjack/metrics"
	mrl "gitlab.utc.fr/hamonsim/ia04_casino/games/Roulette/metrics"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type CasinoAgent struct {
	sync.Mutex
	id string
	//visitors map[AgentID]bool // Permet de savoir si un votant a déjà voté
	addr string // Port sur lequel écouter
}

var resBj float64 = 0.0
var resRl float64 = 0.0

// CREATE a new server
func NewCasinoAgent(addr string) *CasinoAgent {
	return &CasinoAgent{id: addr, addr: addr}
}

func (ca *CasinoAgent) Start() {
	// création du multiplexer
	fs := http.FileServer(http.Dir("./frontend/casino/dist"))
	mux := http.NewServeMux()
	mux.HandleFunc("/", fs.ServeHTTP)
	mux.HandleFunc("/simulate", simulate)
	mux.HandleFunc("/update", update)

	// création du serveur http
	s := &http.Server{
		Addr:           ca.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", ca.addr)
	go log.Fatal(s.ListenAndServe())
}

func simulate(w http.ResponseWriter, request *http.Request) {
	fmt.Println("Method simulate called")

	c1 := make(chan float64)
	c2 := make(chan float64)
	decoder := json.NewDecoder(request.Body)

	var req agt.RequestSimulation
	var res agt.SimulationResponse

	fmt.Println("request", req)
	fmt.Println(req.Trials, req.Strategie, req.BjPlayer)
	go func() { c1 <- mbj.StartSimulation(req.Trials, req.Strategie, req.BjPlayer) }()
	go func() { resBj = <-c1 }()
	go func() { c2 <- mrl.StartSimulation(50, req.StrategieRl, req.RlPlayer, req.Wallet) }()
	go func() { resRl = <-c2 }()
	decoder.Decode(&req)

	res.State = "En cours"

	res.ResultBj = resBj

	fmt.Println(res)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(res); err != nil {
		panic(err)
	}
}

func update(w http.ResponseWriter, request *http.Request) {
	fmt.Println("Method update called")

	var res agt.SimulationResponse

	res.State = "Started"

	if resBj == 0.0 {
		res.ResultBj = float64(rand.Float32())
	} else if math.IsNaN(resBj) {
		res.ResultBj = -1
	} else {
		res.ResultBj = resBj
	}

	if resRl == 0.0 {
		res.ResultRl = float64(rand.Float32())
	} else if math.IsNaN(resRl) {
		res.ResultRl = -1
	} else {
		res.ResultRl = resRl
	}

	fmt.Println(res)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(res); err != nil {
		panic(err)
	}
}
