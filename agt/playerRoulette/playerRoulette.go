package player

import (
	"fmt"
//	"strconv"
	"time"

	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)

type PlayerBase struct {
	Id     agt.AgentID
	Name   string
	Wallet float64
	Resp   chan agt.ReplyRoulette
	Bet    chan agt.RequestBetRoulette
	Play   chan agt.RequestPlayRoulette
	Strat  string
}

func NewPlayerAgent(id agt.AgentID, name string, wallet float64, play chan agt.RequestPlayRoulette, bet chan agt.RequestBetRoulette, Strat string) PlayerBase {
	resp := make(chan agt.ReplyRoulette)
	return PlayerBase{id, name, wallet, resp, bet, play,Strat}
}


func (ag *PlayerBase) GiveBet(bet int) {
	valid := false
	var numbers [37] int
	for valid == false {
		valid = true


		fmt.Println("PLayer : " , ag.Id , " Wallet : ", ag.Wallet ,"I would like to bet :",bet,"$")
		
		//bet = getInteger("How much would you like to bet ($5 increments)? ")
		if bet < 5 {
			valid = false
		}
		if float64(bet) > ag.Wallet {
			valid = false
		}
		if bet%5 != 0 {
			valid = false
		}
		if valid == false {
			fmt.Println("Invalid bet.")
			if ag.Wallet>=5{
				bet=5
			}else{
				bet=0
			}
		}
	}

	 
	for  i:=0;i<=19;i++{
		numbers[i]= 2 + i*2
	}
	values := numbers[0:18]
	ag.Bet <- agt.RequestBetRoulette{ag.Id, bet,values, ag.Resp}

}



func (ag PlayerBase) Start() {
	go func() {
		bet :=int(ag.Wallet/100)
	//	var mu sync.Mutex
		
			

	//	fmt.Printf("You have %.2f in your wallet.\n", ag.Wallet)
		ag.GiveBet(bet)
		answer := <-ag.Resp
		message := answer.Message
		valueWin := answer.AmountWin
		isWin := answer.Win
		fmt.Printf("Player n°%d has received %q  and you won %f\n", ag.Id, message, valueWin) 
		ag.Wallet += valueWin
		time.Sleep(1 * time.Second)
	

		//Labouchere

		playerDone := true
		for playerDone {
			//Martingale
			if ag.Strat=="Martingale"{
				if !isWin{
					bet = 2 *  bet
				}else{
					bet = 5
				}
			}else if ag.Strat=="Impair"{
				//bet =100
			}
		
			
			time.Sleep(20 * time.Millisecond)
			ag.GiveBet(bet)
			reply := <-ag.Resp		
			message = reply.Message
			playerDone = reply.Done
			valueWin := reply.AmountWin
			fmt.Println(reply)
			isWin = reply.Win
			fmt.Printf("Player n°%d has received %q  and you won %f\n", ag.Id, message, valueWin)

			ag.Wallet += valueWin

			if ag.Wallet ==0{
				playerDone=false
			}
		}

		

	}()

}

