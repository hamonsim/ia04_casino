package dealerRoulette

import (
	"fmt"
	//"time"
	crypto_rand "crypto/rand"
    "encoding/binary"
    math_rand "math/rand"
	"sync"
	agtPlayer "gitlab.utc.fr/hamonsim/ia04_casino/agt/playerRoulette"
	agt "gitlab.utc.fr/hamonsim/ia04_casino/types"
)




type Roulette struct{
	Id   agt.AgentID
	Name string
	//Players        []agt.AgentID
	Players        []agtPlayer.PlayerBase
	Bet_players    map[agt.AgentID]int
	Play           chan agt.RequestPlayRoulette
	Bet            chan agt.RequestBetRoulette
	Result		   chan agt.ResultRoulette
	Count_players  map[agt.AgentID]int
	Mu 				sync.Mutex
	Tour			int
	Count 			int
	Roll			int
}



func NewRouletteAgent(id agt.AgentID, name string, players []agtPlayer.PlayerBase, play chan agt.RequestPlayRoulette, bet chan agt.RequestBetRoulette,result chan agt.ResultRoulette) Roulette{
	bet_players := make(map[agt.AgentID]int)
	count_players := make(map[agt.AgentID]int)
	for _, plyr := range players {
		bet_players[plyr.Id] = 0
		count_players[plyr.Id] = -5
	}
	fmt.Println("Roulette creation")
	var mutex sync.Mutex
	return Roulette{id, name, players, bet_players, play, bet,result, count_players, mutex,0,0,-5}
}




func (ag *Roulette) handlePlayerBet(bet agt.RequestBetRoulette) {
	ag.Mu.Lock()
	amount := bet.Bet
	ag.Bet_players[bet.SenderID] += amount
	canContinue := true
	
	if(ag.Count==0){
		ag.Roll = roll()
		ag.Tour++
	}
	ag.Count+=1
	if(ag.Count>=len(ag.Players)){
		ag.Count=0
	}
	if(ag.Tour >100){
		canContinue=false
	}
	fmt.Println("-------------TOUR ", ag.Tour,"-------------")
	win, betWin := play(ag.Roll,amount,bet.Values)
	response := "Votre réponse à bien été prise en compte "
	ag.Count_players[bet.SenderID] += int(betWin)

	Data := agt.ReplyRoulette{
		response,
		betWin,
		win,
		canContinue,
	}
	bet.C <- Data
	
	defer ag.Mu.Unlock()
	
	
	
}


func roll ()(a int){
	var b [8]byte
    _, err := crypto_rand.Read(b[:])
    if err != nil {
        panic("cannot seed math/rand package with cryptographically secure random number generator")
    }
    math_rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
	bt := math_rand.Intn(37)
	return bt
}

func play(valueRoulette int,bet int, numbers []int) (winBool bool,betWin float64) {
	size := len(numbers)
	win :=false
	random := valueRoulette
	var betReturn float64
	fmt.Println("Roulette gives ",random)
	for _,v := range numbers{
		if(v==random){
			win=true
			fmt.Println("ITS A MATCH")
			switch size{
				case 1 :
					//Unique value
					betReturn = float64(36 * bet)
				case 2 :
					//Cheval, deux cases qui se touchent
					betReturn = float64(18 * bet)
				case 3 :
					//Transversale, trois case qui se touchent
					betReturn = float64(12 * bet)
				case 4 :
					//Carré, quatre cases qui se touchent
					betReturn = float64(9 * bet)
				case 6 :
					//Sizain, six cases qui se touchent
					betReturn = float64(6 * bet)
				case 12 :
					//Douzaine , 1-12.13-24.25-36
					betReturn = float64(3 * bet)
				case 18 :
					betReturn = float64(2 * bet)
				case 27 : 
					betReturn = float64(3/2 *bet)
				default:
					fmt.Println("Size is weird", size)
			}
			break
		}
	}
	if win==false{ 
		fmt.Println("Its a lose")
		betReturn =  0.0
	}
	betReturn -=float64(bet)
	return win,betReturn
	
}


func (ag Roulette) Start() {
	go func() {

		//Bet and get values for everyplayer
		ok := true
		for ok {
			select {
			case req, open := <-ag.Bet:
				{
					if !open {
						ok = false
					}
					fmt.Printf("Le croupier n° %d has received a bet of %d from %d\n", ag.Id, req.Bet, req.SenderID)
					go ag.handlePlayerBet(req)
				}
			}
			
			//time.Sleep(1 * time.Second)
		}
	//	time.Sleep(5 * time.Second)
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Bet summary :\n")
		for key, element := range ag.Bet_players {
			fmt.Printf("Player N°%d have bet %d $\n", key, element)
		}
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Values summary :\n")
		sum:=0.0
		for key, element := range ag.Count_players {
		//	fmt.Printf("N°%d: ", key)
			fmt.Printf("Player N°%d have  %d $\n", key, element)
			sum += float64(element)
		}

		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("Wallet Summary\n") 
		for _, plyr := range ag.Players {
			fmt.Printf("N°%d : has %2.f\n", plyr.Id, plyr.Wallet)
			

		}
		
		
		var pourcentSucees float64
		pourcentSucees =  (ag.Players[0].Wallet + (sum/float64(len(ag.Players)))) /ag.Players[0].Wallet
		fmt.Printf("Pourcent sucess of the strategy during this game :%.2f\n", pourcentSucees)
		strat := ag.Players[0].Strat
		data := agt.ResultRoulette{
			strat,
			pourcentSucees,
		}
		ag.Result <- data

	}()
}