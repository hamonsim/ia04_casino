

# IA04 Projet 

Pour lancer le projet "Casino Fever", suivez les étapes suivantes :

1. Cloner le repo git : $git clone https://gitlab.utc.fr/hamonsim/ia04_casino

2. Le frontend en VueJS est déjà inclus dans le fichier (grâce à un build). Lancer ensuite une des commandes suivantes, afin de simuler indépendament les jeux ou alors avec l'interface utilisateur : 

- Pour lancer une simulation de roulette $ go run cmd/launch_roulette/launch_roulette.go
- Pour lancer une simulation de blackjack $ go run cmd/launch_blackjack/launch_blackjack.go
- Pour lancer le serveur backend et frontend $ go run cmd/launch_serveur/launch_serveur.go  

3. Uniquement après la dernière commande, vous avez accès au frontend, pur y accéder rendez vous à l'adresse local : http://127.0.0.1:8000/

4. Vous pouvez changer le port utilisé directement dans le fichier cmd/launch_serveur/launch_serveur.go 



